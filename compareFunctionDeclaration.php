<?php

  function loadMapFile($filename) {
    $ret = [];
    if (file_exists($filename)) {
      $aux = file($filename);
      foreach ($aux as $line) {
        $line = trim(str_replace("\n", " ", $line));
        $line = explode(":", $line);
        $ret[$line[0]]=$line[1];
      }
    }
    return $ret;
  }

  $sourceFile='';
  $targetFile = '';
  $options     = getopt("s:t:h:", array("source:", "target:", "help::"));
  $displayHelp = false;
  foreach ($options as $key => $value) {
    echo "\t$key = $value\n";
    switch ($key) {
      case 'source':
      case 's':
        $sourceFile = $value;
        break;

      case 'target':
      case 't':
        $targetFile = $value;
        break;

      default:
        # code...
        break;
    }
  }

  if ($sourceFile>'' && file_exists($sourceFile)) {
    if ($targetFile>'' && file_exists($targetFile)) {
      $sourceInfo = loadMapFile($sourceFile);
      $targetInfo = loadMapFile($targetFile);

      foreach ($sourceInfo  as $key => $value) {
        echo $key.": ";
        if (!empty($targetInfo[$key])) {
          if ($targetFile[$key] == $value) {
            echo "Identical parameters\n";
          } else {
            echo "Different parameters\n";
          }
        } else {
          echo "not found in target\n";
        }
      }
    } else {
      echo "$targetFile not found\n";
    }
  } else {
    echo "$sourceFile not found\n";
  }