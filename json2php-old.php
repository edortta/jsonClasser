<?php

/**
 * (C) 2020 Esteban D.Dortta
 * MIT license
 * Este script gera uma classe PHP a partir de um JSON Swagger
 * https://gitlab.com/edortta/jsonClasser
 **/

function _getValue($array, $key, $default = null)
{
  if (!empty($array)) {
    if (isset($array[$key])) {
      return $array[$key];
    } else {
      return $default;
    }
  } else {
    return $default;
  }
}

function tirarAcentos($string)
{
  return preg_replace(array("/(á|à|ã|â|ä)/", "/(Á|À|Ã|Â|Ä)/", "/(é|è|ê|ë)/", "/(É|È|Ê|Ë)/", "/(í|ì|î|ï)/", "/(Í|Ì|Î|Ï)/", "/(ó|ò|õ|ô|ö)/", "/(Ó|Ò|Õ|Ô|Ö)/", "/(ú|ù|û|ü)/", "/(Ú|Ù|Û|Ü)/", "/(ñ)/", "/(Ñ)/", "/(ç)/", "/(Ç)/"), explode(" ", "a A e E i I o O u U n N c C"), $string);
}

function garantirNomeUnico($nome, $lista)
{
  if (array_key_exists($nome, $lista)) {
    $ndx = 1;
    while (array_key_exists("$nome$ndx", $lista)) {
      $ndx++;
    }
    $nome = $nome . $ndx;
  }
  return $nome;
}

function criarNomeIdentificador($descriptionName)
{
  global $producerDeclaration;

  $descriptionName = strtolower(tirarAcentos($descriptionName));
  $descriptionName = str_replace("/", "_", $descriptionName);
  $descriptionName = preg_replace('/[[:^alnum:]]/', " ", $descriptionName);
  // $descriptionName = preg_replace('~[\-,\(\)\._\/\[\]]~', ' ', $descriptionName);

  /*
  $descriptionName = str_replace("(", "", $descriptionName);
  $descriptionName = str_replace(")", "", $descriptionName);
  $descriptionName = str_replace(".", " ", $descriptionName);
  $descriptionName = str_replace("-", " ", $descriptionName);
  $descriptionName = str_replace("&", "E", $descriptionName);
   */
  $offset = 0;
  while ($offset < strlen($descriptionName) && $n = strpos($descriptionName, " ", $offset)) {
    $descriptionName = substr($descriptionName, 0, $n) . ucfirst(substr($descriptionName, $n + 1));
    $offset          = $n + 1;
  }

  if (strpos($descriptionName, "(")) {
    die("Erro ao criar o nome do identificador:\n[ $descriptionName ]\n");
  }

  $descriptionName = str_replace(" ", "", $descriptionName);
  $descriptionName = garantirNomeUnico($descriptionName, $producerDeclaration);
  return $descriptionName;
}

function replaceExtension($filename, $new_extension)
{
  $info = pathinfo($filename);
  return $info['filename'] . '.' . $new_extension;
}

function cleanupParamType($paramType)
{
  $paramType = str_replace("<", "", $paramType);
  $paramType = str_replace(">", "", $paramType);

  return $paramType;
}

function cureParamName($paramName, &$originalParamKey)
{
  $originalParamKey = $paramName;
  $offset           = 0;
  while ($offset < strlen($paramName) && $n = strpos($paramName, ".", $offset)) {
    $paramName = substr($paramName, 0, $n) . ucfirst(substr($paramName, $n + 1));
    $offset    = $n + 1;
  }

  if ("$paramName" == "" . intval($paramName)) {
    $paramName = preg_replace('/([0-9]{1,9})/', 'array$1', $paramName);
  }
  return $paramName;
}

function adicionarParametro(
  &$phpDocFuncao,
  &$phpOutput,
  &$workingOnValidParams,
  $firstValidParam,
  $paramName,
  $paramType,
  &$validParamCounter,
  &$param) {

  $paramName = str_replace("[", "_", $paramName);
  $paramName = str_replace("]", "_", $paramName);

  if ($workingOnValidParams) {
    $param[$paramName] = array('type' => $paramType, 'name' => $paramName);
    if ($validParamCounter > 0) {
      $phpOutput .= ", ";
    }
    $phpOutput .= '$' . $paramName;
    $validParamCounter++;
    if (is_array($paramType)) {
      $phpDocFuncao .= "\t * @param json \$$paramName " . json_encode($paramType) . "\n";
    } else {
      $phpDocFuncao .= "\t * @param $paramType \$$paramName\n";
    }

  }

  if ($paramName == $firstValidParam) {
    $workingOnValidParams = true;
  }

}

function produceResponseSample(&$phpBody, $response, $sangria, &$ret)
{
  // die(print_r($response));
  $phpBody .= $sangria . "array(\n";
  foreach ($response as $key => $keyType) {
    $phpBody .= "$sangria\t'$key' => ";
    if (is_array($keyType)) {
      $ret[$key] = array('type' => 'Array', 'ret' => []);
      // die("\n\nKEY: ".json_encode($keyType)."\n\n");
      produceResponseSample($phpBody, $keyType, "$sangria\t", $ret['ret']);
    } else {
      switch ($keyType) {
        case 'String':
          $phpBody .= '""';
          break;

        case 'Int':
          $phpBody .= '0';
          break;

        case 'Float':
          $phpBody .= '0.00';
          break;

        case 'Bool':
          $phpBody .= 'false';
          break;

        default:
          echo "\n---[PHPBODY]------\n";
          echo $phpBody;
          echo "\n---[/PHPBODY]------\n";
          echo "\n---[ERRO]------\n";
          print_r($response);
          echo "\n---[/ERRO]------\n";
          echo "\nUnkwown keyType at '$key': '$keyType'\n";
          debug_print_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
          die();
          break;
      }
      $ret[$key] = array('type' => $keyType);
    }
    $phpBody .= ",\n";

  }
  $phpBody .= $sangria . ")\n";
}

function exportarDeclaracao($item)
{
  global $phpSaida, $asConsumer, $asProducer,
  $privateIdentificator, $producerDeclaration, $author;

  $request            = isset($item['request']) ? $item['request'] : [];
  $method             = $request['method'];
  $url                = $request['url']['raw'];
  $urlHasQuestionMark = strpos("$url", "?");
  if ($urlHasQuestionMark) {
    $url = substr($url, 0, $urlHasQuestionMark);
  }

  $params = [];

  $functionName                       = criarNomeIdentificador($item['name']);
  $producerDeclaration[$functionName] = array(
    'method' => $method,
    'url'    => $url,
    'params' => [],
    'ret'    => []);

  echo "Name: " . $item['name'] . "\n";
  echo "Function Name: " . $functionName . "\n";
  echo "URL: " . $url . "\n";
  echo "Method: " . $method . "\n";
  echo "Request: \n";

  $phpDocFuncao =
  "\n\t * " . $item['name'] . "( $url )" .
  "\n\t * @author $author" .
  "\n\t * @since " . date("Y-m-d H:i:s") .
    "\n\t * @entrypoint ($method) $url" .
    "\n\t * @todo FALTA IMPLEMENTAR" .
    "\n\t * @access public\n";

  $phpDeclaracaoFuncao = "\n  public function $functionName(";

  /*
  na url, o primeiro parámetro com ':' indica uma chamada privada
  logo esse parâmetro não é recebido como parâmetro pela função
   */
  $workingOnValidParams = true;
  $validParamCounter    = 0;
  $cc                   = substr_count($url, ":");
  $firstValidParam      = "";
  $isPrivateCall        = false;
  if ($asConsumer) {
    if ($cc > 0) {
      $n                    = strpos($url, ":") + 1;
      $firstValidParam      = substr($url, $n);
      $firstValidParam      = substr($firstValidParam, 0, strpos("$firstValidParam/", "/"));
      $workingOnValidParams = ($urlHasQuestionMark) || ($firstValidParam == "");
      $isPrivateCall        = ($privateIdentificator > '') && ($firstValidParam == $privateIdentificator);
    }
  }

  preg_match_all('/:([a-zA-Z0-9_]*)/', $url, $auxHiddenParams);
  $hiddenParams = $auxHiddenParams[1];

  $reqCount = 0;

  if ($asProducer) {
    foreach ($hiddenParams as $paramKey => $paramName) {
      $paramName = cleanupParamType($paramName);
      $paramName = cureParamName($paramName, $originalParamKey);
      $paramName = garantirNomeUnico($paramName, $params);
      $paramType = "String";
      echo "\t$paramName* : $paramType\n";
      $params[$paramName] = ['type' => $paramType, 'name' => $originalParamKey];

      adicionarParametro($phpDocFuncao, $phpDeclaracaoFuncao, $workingOnValidParams, $firstValidParam, $paramName, $paramType, $validParamCounter, $producerDeclaration[$functionName]['params']);
      $reqCount++;
    }
  }

  if ($method == 'POST') {
    $workingOnValidParams = true;
    $body                 = @$request['body'];
    if ($body) {
      if (isset($body['formdata'])) {
        $formdata = $body['formdata'];
        for ($i = 0; $i < count($formdata); $i++) {
          $param     = $formdata[$i];
          $paramKey  = cureParamName($param['key'], $originalParamKey);
          $paramKey  = garantirNomeUnico($paramKey, $params);
          $paramType = cleanupParamType($param['value']);
          if (!in_array($paramKey, $hiddenParams)) {
            echo "\t$paramKey : $paramType\n";
            $params[$paramKey] = ['type' => $paramType, 'name' => $originalParamKey];

            adicionarParametro($phpDocFuncao, $phpDeclaracaoFuncao, $workingOnValidParams, $firstValidParam, $paramKey, $paramType, $validParamCounter, $producerDeclaration[$functionName]['params']);
            $reqCount++;
          }
        }
      } else if (isset($body['raw'])) {
        $query = json_decode($body['raw'], 1);
        foreach ($query as $paramKey => $paramType) {
          $paramKey  = cureParamName($paramKey, $originalParamKey);
          $paramKey  = garantirNomeUnico($paramKey, $params);
          $paramType = cleanupParamType($paramType);
          if (!in_array($paramKey, $hiddenParams)) {
            if (is_array($paramType)) {
              echo "\t$paramKey : " . json_encode($paramType) . "\n";
            } else {
              echo "\t$paramKey : $paramType\n";
            }

            $params[$paramKey] = ['type' => $paramType, 'name' => $originalParamKey];

            adicionarParametro($phpDocFuncao, $phpDeclaracaoFuncao, $workingOnValidParams, $firstValidParam, $paramKey, $paramType, $validParamCounter, $producerDeclaration[$functionName]['params']);
            $reqCount++;
          }
        }
      }
    }
  } else {
    /* GET */
    $urlRequest = $request['url'];
    $query      = [];
    if (isset($urlRequest['query'])) {
      $query = $urlRequest['query'];
    } else if (isset($urlRequest['variable'])) {
      $query = $urlRequest['variable'];
    }

    for ($i = 0; $i < count($query); $i++) {
      $paramKey         = @$query[$i]['key'];
      $paramKey         = cureParamName($paramKey, $originalParamKey);
      $paramKey         = garantirNomeUnico($paramKey, $params);
      $paramDescription = @$query[$i]['description'];
      $paramType        = cleanupParamType(@$query[$i]['value']);

      $params[$paramKey] = ['type' => $paramType, 'name' => $originalParamKey];
      if (!in_array($paramKey, $hiddenParams)) {

        echo "\t$paramKey : ( $paramType ) /* $paramDescription */\n";

        adicionarParametro($phpDocFuncao, $phpDeclaracaoFuncao, $workingOnValidParams, $firstValidParam, $paramKey, $paramType, $validParamCounter, $producerDeclaration[$functionName]['params']);
        $reqCount++;
      }
    }

  }

  if ($asConsumer) {

    if ($reqCount == 0) {
      echo " ( NO PARAMS) \n";
    }

    if ($isPrivateCall) {
      $phpBody = "\n\t\t" . '$ret = $this->privateRequest(';
    } else {
      $phpBody = "\n\t\t" . '$ret = $this->publicRequest(';
    }
    $phpBody .= "\n\t\t" . "\"$method\",";
    $auxURL = $url;

    if (is_array($auxURL)) {
      echo "Uepa.......\n";
      die(print_r($auxURL));
    }

    preg_match_all('/:[a-zA-Z_0-9]*/', $auxURL, $auxParams);

    foreach ($auxParams[0] as $key) {
      $asParam = str_replace(':', '$', $key);
      $auxURL  = str_replace($key, $asParam, $auxURL);
    }
    if (($privateIdentificator > "") && (strpos($auxURL, $privateIdentificator) > 0)) {
      $auxURL = substr($auxURL, strpos($auxURL, $privateIdentificator) + 14);
    } else {
      if (preg_match('/(.*\/v[0-9])(\/.*)/', $auxURL)) {
        preg_match_all('/(.*\/v[0-9])(\/.*)/', $auxURL, $auxSplitedURL);
        if ($auxSplitedURL) {
          $auxURL = $auxSplitedURL[2][0];
        }
      } else {
        preg_match_all('/(\/.*)/', $auxURL, $auxSplitedURL);
        if ($auxSplitedURL) {
          $auxURL = $auxSplitedURL[1][0];
        }
      }
    }

    $phpBody .= "\n\t\t" . '"' . $auxURL . '",';
    $phpBody .= "\n\t\t" . '[';
    foreach ($params as $paramCuredName => $paramTypeDef) {
      $paramType  = $paramTypeDef['type'];
      $paramName  = $paramTypeDef['name'];
      $paramName1 = str_replace("[", "_", $paramName);
      $paramName1 = str_replace("]", "_", $paramName1);

      if (!in_array($paramName, $hiddenParams)) {
        $phpBody .= "\n\t\t\t'$paramName' => \$$paramCuredName,";
      }
    }
    $phpBody .= ']);' . "\n\t\treturn \$ret;\n\t";
  } else {
    $phpBody = "\n\t\$ret = ";
    produceResponseSample($phpBody, $item['response'], "\t\t", $producerDeclaration[$functionName]['ret']);
    $phpBody .= ";\n\t/* You need to change this when implemented */\n\thttp_response_code(501);\n\treturn \$ret;\n";
  }
  fwrite($phpSaida, "\n\t/*$phpDocFuncao\t */$phpDeclaracaoFuncao" . "){" . $phpBody . "}\n");

  echo "\n";
}

function exportarDefinicao($item)
{
  $ret = false;
  if ($item) {
    if (isset($item['item'])) {
      exportarDefinicao($item['item'][0]);
    } else {
      for ($i = 0; $i < count($item); $i++) {
        if (isset($item[$i]['item'])) {
          exportarDefinicao($item[$i]['item']);
        } else {
          exportarDeclaracao($item[$i]);
        }
      }
    }
  }
  return $ret;
}

function exportarDeclaracoesProdutor($params)
{
  global $phpSaida;

  echo "Productor Declarations\n";
  print_r($params);
  foreach ($params as $key => $value) {
    $funcParams = json_encode($value['params']);
    $funcUrl    = $value['url'];
    $funcMethod = $value['method'];
    fwrite($phpSaida, "\t\t\$this->registerEntry('$key', '$funcMethod', '$funcUrl', '$funcParams');\n");
  }
}

$isCLI = (php_sapi_name() == "cli");

if (isset($_REQUEST['file'])) {
  $nomeArquivo = @$_REQUEST['file'];
} else {
  $asConsumer           = false;
  $asProducer           = false;
  $privateIdentificator = '';
  $classIdentifier      = '';
  $author               = '';
  $nomeArquivo          = '';

  $options     = getopt("a:f:i:k:p:h:c:", array("author::", "file::", "consumer::", "producer::", "identifier::", "class:", "help:"));
  $displayHelp = false;
  foreach ($options as $key => $value) {
    echo "\t$key: $value\n";
    switch ($key) {
      case 'i':
      case 'identifier':
        $privateIdentificator = $value;
        break;

      case 'a':
      case 'author':
        $author = $value;
        break;

      case 'c':
      case 'class':
        $classIdentifier = $value;
        break;

      case 'f':
      case 'file':
        $nomeArquivo = $value;
        break;

      case 'k':
      case 'consumer':
        $asConsumer = true;
        break;

      case 'p':
      case 'producer':
        $asProducer = true;
        break;

      case 'h':
      case 'help':
        $displayHelp = true;
        break;

      default:
        die("$key é um parâmetro desconhecido\n");
        break;
    }
  }
  if ((($asConsumer || $asProducer) == false) ||
    ($classIdentifier == '') ||
    ($nomeArquivo == '') ||
    ($displayHelp)
  ) {
    echo basename($argv[0]) . "\n\t-h\tEsta ajuda\n\t-a\t<autor>\n\t-f\t<arquivo>\n\t[--consumer]\n\t[--producer]\n\t-i <identificador> (separa chamadas públicas de privadas)\n\t-c <classe>\n";
    if ($classIdentifier == '') {
      echo "Indique a classe anterior\n";
    }

    if (($asConsumer || $asProducer) == false) {
      echo "Indique se é um consumidor ou produtor\n";
    }

    exit(1);
  }
  $nomeArquivo = @$options['f'];
}

if (!file_exists($nomeArquivo)) {
  echo "Indique qual arquivo quer processar\n";
  $base = basename(__FILE__);
  foreach (glob("*.json") as $key => $value) {
    if (!$isCLI) {
      echo "<br><a href='$base?file=$value'>";
    }
    echo "$value\n";
    if (!$isCLI) {
      echo "</a>\n";
    }

  }
} else {
  if (!$isCLI) {
    echo "<PRE>";
  }
  $producerDeclaration = [];

  $nomeArquivoPHP = replaceExtension(criarNomeIdentificador($nomeArquivo), "php");
  $nomeClasse     = ucfirst(substr($nomeArquivoPHP, 0, strpos($nomeArquivoPHP, ".")));

  echo "Processando: " . $nomeArquivo . "\n";
  echo "Criando: " . $nomeArquivoPHP . "\n";
  echo "Classe: " . $nomeClasse . "\n";
  echo str_repeat("-", 13 + strlen($nomeArquivo)) . "\n";
  $json = @file_get_contents($nomeArquivo);
  if ($json) {
    $assoc = json_decode($json, true);

    if ($assoc) {
      $phpSaida = fopen($nomeArquivoPHP, "w");
      fwrite($phpSaida, "<?php\n\nclass $nomeClasse" . " extends $classIdentifier {\n");
      exportarDefinicao(_getValue($assoc, 'item', []));
      fwrite($phpSaida, "\tfunction __construct() {\n");
      if ($asProducer) {
        exportarDeclaracoesProdutor($producerDeclaration);
      }
      fwrite($phpSaida, "\t\tparent::__construct();\n\t}\n");

      fwrite($phpSaida, "\n}\n\nglobal \$api;\n\$api = new $nomeClasse();\n\n?>");
      fclose($phpSaida);
      chmod($nomeArquivoPHP, 0755);
    }
  } else {
    echo "Arquivo não pode ser processado\n";
  }
}
