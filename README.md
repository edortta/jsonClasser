# JsonClasser

This project now has two scripts: json2php and phpFunctionExtractor

json2php has the task of transform Swagger (POSTMAN) Json files into PHP classes at a snap

#### Usage:

1) Using POSTMAN, export your collection to a "*Collection v2.1"* JSON file

2) Using json2php convert JSON file in PHP class

```bash
$ php json2php --consumer -c Test -f test.json -a "Daniel Pepper"
```

#### Creating a producer

You can create a producer class if you have the API definition. That is the case when you need to implement an interface that emulates another server you don't have acces to.

```bash
$ php json2php  -a "Daniel Pepper" --producer -c Test -f test.json
```

#### Splitting private calls from public calls

Sometimes you need to do differents calls. Some parts of the API are public while others are private. It's a common place to have a splitter in the URL that identifies that situation.

Let's say your splitter is :tokenid

```bash
$ php json2php --consumer  -a "Daniel Pepper" -c Test -f test.json -i ":tokenid"
```

That's all

-----

You can collaborate with me in creating free software:

https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=3UKC9GHT6CKN4&source=url

![title](QR Code.png)

---

Meet YeAPF https://www.YeAPF.com
