#!/bin/bash

php=`which php`
php_exists=`echo $?`
if [ $php_exists -eq 0 ]; then
  cant=1
  dist=`uname -s`
  dist=${dist:0:6}

  tmp=`echo $TEMP$TMPDIR`
  if [[ "$tmp" == "" ]]; then
    tmp="/tmp"
  fi

  if [[ "$dist" == "CYGWIN" ]]; then
    if [ ! -f /usr/bin/php ]; then
      sed 's|%PHP%|'$php'|g' cygwinPHP.sh > /usr/bin/php
      chmod +x /usr/bin/php
      php=`which php`
    fi
    cant=0
  fi

  if [[ $EUID -eq 0 ]]; then
    cant=0
  fi

  if [[ $cant -ne 0 ]]; then
    echo "You must be root to run this script. Aborting...";
    exit 1;
  fi

  first_place=`echo $PATH | cut -d\: -f1`
  echo "Installing in $first_place"
  sed 's|%PHP%|'$php'|g' json2php | sed 's|%LOCATION%|'$first_place'|g' > "$first_place/json2php"
  chmod +x "$first_place/json2php"
  cp json2php.php "$first_place/"
  echo "You can now run json2php from your console"
else
  echo "We need PHP in order to run jsonclasser"
fi