<?php
  /**
   * This script helps to discover when a call to a function has changed
   * from version to version.
   * For some reason it's possible you may had reformated your source code and,
   * for example, added some lines between parameters in a function declaration.
   * If the class is small, you can just check using your eyes
   * but if it's long, you need a tool like this.
   * It's a very simple script in the way that it does not pay attention to classes
   **/

  function processFile($filename) {
    $ret = false;
    if (file_exists($filename)) {
      $fileContent = file_get_contents($filename);
      /* remove NEW_LINE */
      $fileContent = str_replace("\n", " ", $fileContent);
      /* I'm not sure if the CR is changed in Windows version */
      $fileContent = str_replace("\r", " ", $fileContent);

      /* Translate TAB chars tospaces */
      $fileContent = str_replace("\t", " ", $fileContent);

      /* let's put the 'function' tag at the start of the lines */
      $fileContent = str_replace("function", "\nfunction", $fileContent);

      /* let's extract functions definitions */
      preg_match_all('/function[\ ]*([a-zA-Z_]{1}[0-9a-zA-Z_]*)[\ ]*\([\ ]*([\$\&]*([a-zA-Z_]{1}[0-9a-zA-Z_]*)[\,\ ]*){0,99}\)/', $fileContent, $output_array);
      if ($output_array) {
        // die(print_r($output_array));
        $ret = [];
        $functions = $output_array[0];
        foreach ($functions as $definiton) {
          preg_match_all('/function[ ]*([a-zA-Z_]{1}[a-zA-Z0-9_]{0,99})[\ ]*\(/', $definiton, $funcName);
          $functionName = $funcName[1][0];
          $ret[$functionName] = [];
          preg_match_all('/[\$\&]{1,2}[a-zA-Z_]{1}[a-zA-Z_0-9]*/', $definiton, $params);

          foreach ($params[0] as $key) {
            if (!empty($key)) {
              $param = $key;
              if (substr($param,0,2)=='&$')
                $paramType='pointer';
              else
                $paramType='value';
              $param = preg_replace('/([&\$]*)([a-zA-Z_]{1}[a-zA-Z0-9_]*)/', '$2', $param);
              $ret[$functionName][$param]=$paramType;

            }
          }
        }
        // $ret = json_encode($ret);
      }

    }
    return $ret;
  }


  if ($argc<2) {
    die("Indicate at least one file\n");
  }

  $ret = [];

  for($c=1; $c<$argc; $c++) {
    $aux = processFile($argv[$c]);
    $ret = array_merge($ret, $aux);
  }

  ksort($ret);
  foreach ($ret as $function => $params) {
    echo "$function: ";
    foreach($params as $paramName=>$paramType) {
      if ($paramType=='pointer')
        echo "*";
      echo $paramName.' ';
    }
    echo "\n";
  }