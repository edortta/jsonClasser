<?php

/**
 * This ugly and dirty tools is the responsible for
 * translate openapi into php class
 * (C) 2019-2022 Esteban D.Dortta
 */
$GLOBALS['debug_0'] = false;
if (!function_exists('arrayKeyFirst')) {
  function arrayKeyFirst(array $array) {
    if (count($array)) {
      reset($array);
      return key($array);
    }
    return null;
  }
}

function _showStack() {
  $stack = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
  for ($i = 1; $i < count($stack); $i++) {
    echo str_pad($stack[$i]['line'], 5, " ", STR_PAD_LEFT) . " => " . $stack[$i]['function'] . "()\n";
  }
}

function removeDiacritics($string) {
  return preg_replace(array("/(á|à|ã|â|ä)/", "/(Á|À|Ã|Â|Ä)/", "/(é|è|ê|ë)/", "/(É|È|Ê|Ë)/", "/(í|ì|î|ï)/", "/(Í|Ì|Î|Ï)/", "/(ó|ò|õ|ô|ö)/", "/(Ó|Ò|Õ|Ô|Ö)/", "/(ú|ù|û|ü)/", "/(Ú|Ù|Û|Ü)/", "/(ñ)/", "/(Ñ)/", "/(ç)/", "/(Ç)/"), explode(" ", "a A e E i I o O u U n N c C"), $string);
}

global $idPaths;
$idPaths = [
  'POST'   => [],
  'GET'    => [],
  'DELETE' => [],
  'PUT'    => [],
  'PATCH'  => [],
];
function addPath($method, $path) {
  global $idPaths;
  $ret    = false;
  $method = mb_strtoupper($method);
  if (isset($idPaths[$method])) {
    if (!isset($idPaths[$method][$path])) {
      $idPaths[$method][$path] = true;
      $ret                     = true;
    }
  }
  return $ret;
}

function grantUniqueIdInList($id, $list) {
  if (array_key_exists($id, $list)) {
    $ndx = 1;
    while (array_key_exists("$id$ndx", $list)) {
      $ndx++;
    }
    $id = $id . $ndx;
  }
  return $id;
}

global $idList;
$idList = [];

function createIdentifierFromDescription($description, $grantIsUnique = true) {
  global $idList;

  $p           = 0;
  $description = preg_replace('/\'/', ' ', $description);
  $description = strtolower(removeDiacritics($description));
  $description = " " . trim($description);
  do {
    $p0 = strpos($description, '/');if (!$p0) {
      $p0 = strlen($description);
    }

    $p1 = strpos($description, '-');if (!$p1) {
      $p1 = strlen($description);
    }

    $p2 = strpos($description, ' ', 1);if (!$p2) {
      $p2 = strlen($description);
    }

    $p = min($p0, $p1, $p2);
    if ($p == strlen($description)) {
      $p = false;
    }

    if ($p) {
      $description = substr($description, 0, $p) . ucfirst(substr($description, $p + 1));
    }
  } while ($p);
  $description = lcfirst(trim($description));
  $description = trim(preg_replace('/[[:^alnum:]]/', " ", $description));

  if (strpos($description, "(")) {
    die("Error creating identifier name:\n[ $description ]\n");
  }

  if (preg_match('/^[0-9]/', $description, $output_array)) {
    $description = "_$description";
  }

  if ($grantIsUnique) {
    $description = grantUniqueIdInList($description, $idList);
  }

  return $description;
}

function replaceExtension($filename, $new_extension) {
  $info = pathinfo($filename);
  return $info['dirname'] . '/' . $info['filename'] . $new_extension;
}

function _getValueWithCallback($array, $key, $default = null, $callbackNotDefault = null, $callbackParam = '') {
  if (!empty($array)) {
    if (isset($array[$key])) {
      if ($callbackNotDefault) {
        $callbackNotDefault(trim($callbackParam . ' ' . $array[$key]));
      }

      return $array[$key];
    } else {
      return $default;
    }
  } else {
    return $default;
  }
}

function _getValue($array, $key, $default = null) {
  return _getValueWithCallback($array, $key, $default);
}

/**
 * Output low level management
 **/
global $fOutput;
function openOutput($fileName) {
  global $fOutput;
  $fOutput = fopen($fileName, "w");
  if ($fOutput) {
    $ret = true;
  } else {
    $ret = false;
    die("File '$fileName' cannot be created\n");
  }
  return $ret;
}

function closeOutput() {
  global $fOutput;
  if ($fOutput) {
    fclose($fOutput);
    $fOutput = null;
  }
}

global $margin;
$margin = 0;
function genOutput($strLine) {
  global $fOutput, $margin;

  if ($fOutput) {
    if (trim($strLine) > '') {
      fwrite($fOutput, str_repeat(" ", $margin) . rtrim($strLine) . "\n");
    }

  } else {
    die("File not created\n");
  }
}

/**
 * Output middle level management
 **/
global $intoComment, $commentCount, $marginDeep;
$intoComment  = false;
$commentCount = 0;
$marginDeep   = 1;

define("WORDWRAP_COLUMN", 72);

$genComment = function ($value, $asSecondaryComment = false) {
  global $genComment, $margin, $secondMargin, $intoComment, $commentCount, $marginDeep;

  if (strpos($value, "\n")) {
    $auxSecMarg     = 0;
    $auxValue       = explode("\n", $value);
    $oldIntoComment = $intoComment;
    foreach ($auxValue as $v) {
      $genComment($v, !$intoComment);
      $intoComment = false;
    }
    $intoComment = $oldIntoComment;
  } else {
    $marginAux = 0;
    if (substr($value, 0, 1) == '@') {
      $aux = $marginDeep;
      while ($aux-- > 0) {
        $marginAux = strpos($value, " ", $marginAux) + 1;
      }
    }
    if ($intoComment && $commentCount++ > 0) {
      genOutput(" *");
    }

    if (($value != '/**') && ($value != ' */')) {
      $firstColumn = intval(max(3, $secondMargin) - 3) * intval($asSecondaryComment);

      $aux = " * " . wordwrap(str_repeat(" ", $firstColumn) . $value, max(40, WORDWRAP_COLUMN - $margin), "\n" . str_repeat(" ", $margin) . " * " . str_repeat(" ", $marginAux + intval($secondMargin) * intval($asSecondaryComment)));
      genOutput($aux);

      if (!$asSecondaryComment) {
        $secondMargin = $marginAux + 2;
      }

    } else {
      genOutput($value);
    }
  }
};

$openComment = function () {
  global $intoComment, $genComment, $commentCount;
  if (!$intoComment) {
    $commentCount = 0;
    $genComment("/**");
    $intoComment = true;
  }
};

$closeComment = function () {
  global $intoComment, $genComment;
  if ($intoComment) {
    $intoComment = false;
    $genComment(" */");
  }
};

function _getItemTypeOrObject($value, &$valueSet) {
  $ret = _getValue(
    $value, 'type',
    _getValue(
      $value, 'allOf',
      _getValue(
        $value, 'anyOf',
        _getValue(
          $value, 'oneOf', 'UNKNOWN'))));
  if ($ret == 'UNKNOWN') {
    $ret = _getValue($value, 0, 'UNKNOWN2');
    if ($ret == 'UNKNOWN2') {
      if (!empty($value['$ref'])) {
        $ret = 'ObjectSchema';
      }
    }
  }

  $valueSet = [];

  if (!empty($value['type'])) {
    $valueSet = $value;
  }

  if (!empty($value['allOf'])) {
    $valueSet = $value['allOf'];
  }

  if (!empty($value['anyOf'])) {
    $valueSet = $value['anyOf'];
  }

  if (!empty($value['oneOf'])) {
    $valueSet = $value['oneOf'];
  }

  return $ret;
}

function genSchemaParamComment($schemaDefinition, &$secondaryDescription, $withKey = false) {
  global $margin;
  $secondaryDescription = _getValue($schemaDefinition, 'description2', _getValue($schemaDefinition, 'description', ''));
  if (is_array($secondaryDescription)) {
    $secondaryDescription = _getValue($secondaryDescription, 'description2', _getValue($secondaryDescription, 'description', ''));
  }
  $pDesc = '';

  $addComma = function () use (&$pDesc, &$secondaryDescription) {
    if ($pDesc > '') {
      $pDesc .= ", ";
    }

    if ($secondaryDescription > '') {
      $secondaryDescription .= "\n";
    }
  };

  foreach (_getValue($schemaDefinition, 'properties', $schemaDefinition) as $key => $value) {
    if (is_array($value)) {
      if (isset($value['$ref'])) {
        $auxDesc          = _getValue($value, 'description2', _getValue($value, 'description', ''));
        $schemaDefinition = getSchemaDefinition($value);
        // print_r("\n$key\n");
        // print_r($value);
        // print_r($schemaDefinition);
        $addComma();
        $pDesc .= ($withKey ? "$key: " : '') . str_replace("\n", " ", genSchemaParamComment($schemaDefinition, $auxDesc));
        // die("\n$pDesc\n");
        $secondaryDescription .= $auxDesc;
      } else {
        $itemTypeOrObject = _getItemTypeOrObject($value, $valueSet);
        if (is_string($itemTypeOrObject)) {
          $addComma();
          $aType = $itemTypeOrObject;
          $pDesc .= $key . ':';
          if ($aType == 'object') {
            $auxSecDesc = '';
            $margin += 2;
            $pDesc .= genSchemaParamComment($value, $auxSecDesc);
            $secondaryDescription .= $auxSecDesc;
            $margin -= 2;
          } else {
            $pDesc .= $aType;

            $enumDesc = '';
            $enum     = _getValue($value, 'enum', false);
            if ($enum) {
              $enumDesc = '';
              foreach ($enum as $enumKey) {
                if ($enumDesc > '') {
                  $enumDesc .= ", ";
                }

                $enumDesc .= $enumKey;
              }
              if ($enumDesc > '') {
                $enumDesc = "Enum ( $enumDesc )";
              }

            }

            $auxDesc = trim(_getValue($value, 'description2', _getValue($value, 'description', '')) . ' ' . $enumDesc);
            if ($auxDesc > '') {
              $auxDesc = '[' . $key . "]: " . $auxDesc;
              $secondaryDescription .= $auxDesc;
            }
          }
        } else {
          if (is_array($valueSet)) {
            foreach ($valueSet as $key => $value) {
              $pDesc .= genSchemaParamComment($value, $auxSecDesc);
              $secondaryDescription .= $auxSecDesc;
            }
          } else {
            echo "\nValueSet\n";
            print_r($valueSet);
            echo "\nKey\n";
            echo "\t'$key'";
            echo "\nValue\n";
            print_r($value);
            echo "\n======\n";
            print_r($schemaDefinition);
            _showStack();
            die("What!?\n");
          }
        }
      }
    }
  }
  if (trim($pDesc) > '') {
    $pDesc = "{ " . str_replace(",", ",\n  ", $pDesc) . " }";
  }

  return $pDesc;
};

function genParamComment($parametersDef) {
  global $genComment;
  foreach ($parametersDef as $key => $value) {
    $schemaDefinition = [];
    if (isset($value['$ref'])) {
      $schemaDefinition = getSchemaDefinition($value);
    }

    echo "\t$key\t";
    if ([] != $schemaDefinition) {
      $pType = _getValue($schemaDefinition, 'type', 'object');
      echo "$pType " . __LINE__ . "\n";

      if ($pType == 'object') {
        $secondaryDescription = '';
        $pDesc                = genSchemaParamComment($schemaDefinition, $secondaryDescription);
        if (trim($secondaryDescription) > '') {
          $pDesc .= str_replace("\n", "\n  ", "\n$secondaryDescription");
        }

      } else {
        $pDesc = _getValue($schemaDefinition, 'description2', _getValue($schemaDefinition, 'description', ''));
      }

    } else {
      if (isset($value['type']) && isset($value['description'])) {
        $pType = _getValue($value, 'type', 'object');
        $pDesc = _getValue($value, 'description2', _getValue($value, 'description', ''));
        echo "$pType " . __LINE__ . "\n";
      } else {
        $pType                = _getValue($value, 'type', 'object');
        $secondaryDescription = '';
        if ($pType == 'array') {
          $schemaDefinition = getSchemaDefinition($value['items']);
          $pDesc            = genSchemaParamComment($schemaDefinition, $secondaryDescription, true);
          $pDesc            = "[ $pDesc ]";
          // echo "\npDesc: $pDesc\nsecondaryDescription: $secondaryDescription\n";
          // die(print_r($schemaDefinition));
        } else {
          $pDesc = genSchemaParamComment($value, $secondaryDescription);
        }
        if (trim($secondaryDescription) > '') {
          $pDesc .= str_replace("\n", "\n  ", "\n$secondaryDescription");
        }
        echo "$pType " . __LINE__ . "\n";
      }
    }
    if (is_array($pDesc)) {
      $pDesc = _getValue($pDesc, 'type', 'string');
    }

    $genComment("@param " . trim($pType . ' $' . $key . ' ' . $pDesc));
  }
};

function genInfoOutput($info) {
  global $genComment;

  genOutput("/**");
  _getValueWithCallback($info, "title", "", $genComment, "@title");
  _getValueWithCallback($info, "version", "", $genComment, "@version");
  _getValueWithCallback($info, "description", "", $genComment, "@description");

  $license = _getValue($info, 'license', []);
  _getValueWithCallback($license, "name", "", $genComment, "@licenseName");
  _getValueWithCallback($license, "url", "", $genComment, "@licenseURL");

  $contact = _getValue($info, 'contact', []);
  _getValueWithCallback($contact, "email", "", $genComment, "@contactEmail");
  _getValueWithCallback($contact, "url", "", $genComment, "@contactURL");

  genOutput(" * @date " . date("Y-m-d H:i:s"));
  genOutput(" **/");
}

function genArrayPayloadOutput($array) {
  global $margin;
  if ("NULL" == $array) {
    $array = null;
  }

  if (null !== $array) {
    if (is_array($array)) {
      foreach ($array as $key => $value) {
        $type  = _getValue($value, 'type', 'UNKNOWN3');
        $value = _getValue($value, 'value', 'NULL');
        if ($type == 'string') {
          $value = "'$value'";
        } else {
          if ($type == 'boolean') {
            $value = ($value ? 'true' : 'false');
          }
        }

        if ($type == 'SchemaObject' || $type == 'array') {
          genOutput("'$key' => [");
          $margin += 2;
          genArrayPayloadOutput($value);
          $margin -= 2;
          genOutput("], ");
        } else {
          genOutput("'$key' => $value,");
        }
      }
    } else {
      echo "\n" . __LINE__ . "\n";
      die(var_dump($array));
    }
  }
}

function genClassHeader($targetFilename, $jSource, $parentClassName = '') {
  global $margin, $className, $basicReturnClassName, $asConsumer, $asProducer;
  $ret     = false;
  $version = _getValue($jSource, "openapi", "none");
  if (substr($version, 0, 1) == '3') {
    if (openOutput($targetFilename)) {
      genOutput("<?php\n");

      if ($basicReturnClassName == '') {
        $basicReturnClassName = "EmptyRet" . date("YmdHi");
        genOutput("class $basicReturnClassName {");
        $margin += 2;
        genOutput("public \$http_code;");
        genOutput("public \$error_code;");
        genOutput("public \$message;");
        genOutput("public \$content;");
        genOutput("public \$contentType;");
        genOutput("function __construct(\$http_code=204, \$message='', \$error_code=0, \$content=[], \$contentType='application/json') {");
        $margin += 2;
        genOutput("\$this->http_code  = \$http_code;");
        genOutput("\$this->error_code = \$error_code;");
        genOutput("\$this->message  = \$message;");
        genOutput("\$this->content  = json_encode(\$content, JSON_PRETTY_PRINT);");
        genOutput("\$this->contentType  = \$contentType;");
        $margin -= 2;
        genOutput("}");

        $margin -= 2;
        genOutput("}");
      }

      $info = _getValue($jSource, "info", []);
      genInfoOutput($info);

      $className = createIdentifierFromDescription(replaceExtension("$targetFilename", ""));

      if ($parentClassName > '') {
        genOutput("class $className extends $parentClassName {");
      } else {
        genOutput("class $className {");
      }

      $margin += 2;
      genOutput("private \$__api;");

      $ret = true;
    } else {
      echo "File $targetFilename cannot be created";
    }
  } else {
    echo "Version '$version' not known\n";
    echo "Maybe you would like to convert it in OpenAPI 3.x\n";
  }

  return $ret;
}

function getTags($jSource) {
  global $tags;
  $auxTags = _getValue($jSource, 'tags', []);
  $tags    = [];
  foreach ($auxTags as $aTag) {
    $tags[$aTag['name']] = ['declared' => false, 'description' => $aTag['description']];
  }
}

function getSchemaDefinition($schema) {
  global $components;
  $schemaDefinition = [];
  $ref              = _getValue($schema, '$ref', '');
  if ($ref > '') {
    preg_match_all('/#\/([a-zA-Z]*)\/([a-zA-Z]*)\/(.*)/', $ref, $output_array);
    if ($output_array) {
      if ($output_array[1][0] == 'components') {
        $aux              = _getValue($components, $output_array[2][0], []);
        $schemaDefinition = $aux[$output_array[3][0]];
        // if ($ref=='#/components/schemas/PaymentType')
        //   die(print_r($schemaDefinition));
      } else {
        echo "$ref not found\n";
      }
    } else {
      echo "$ref not defined\n";
    }
  } else {
    if (isset($schema['type'])) {
      $schemaDefinition = $schema;
    } else {
      echo "\n" . __LINE__ . " How to proceed?\n";
      die(print_r($schema));
    }
  }
  return $schemaDefinition;
}

function genPayloadFromSchemaObject($schema) {
  // https://swagger.io/specification/#schema-object
  $ret = [];
  foreach (_getValue($schema, 'properties', []) as $key => $value) {
    $itemTypeOrObject = _getItemTypeOrObject($value, $valueSet);
    if (is_string($itemTypeOrObject)) {
      if ($itemTypeOrObject == 'object') {
        $ret[$key] = genPayloadFromSchemaObject($value);
      } else {
        if ($itemTypeOrObject == 'ObjectSchema') {
          $auxSchema          = getSchemaDefinition($value);
          $ret[$key]          = ['type' => 'array'];
          $ret[$key]['value'] = genPayloadFromSchemaObject($auxSchema);
        } else {
          $ret[$key] = [
            'type'        => $itemTypeOrObject,
            'description' => _getValue($value, 'description', ''),
            'value'       => _getValue($value, 'example', null),
            '_debug'      => json_encode($value),
          ];
        }
      }
    } else {
      $ret[$key] = [
        'type'   => 'SchemaObject',
        'object' => $itemTypeOrObject,
      ];
      $ret[$key]['value'] = [];
      if (!empty($value['allOf'])) {
        $ret[$key]['subType'] = 'allOf';
        foreach ($value['allOf'] as $schema2) {
          $auxSchema  = getSchemaDefinition($schema2);
          $auxPayload = genPayloadFromSchemaObject($auxSchema);
          foreach ($auxPayload as $plk1 => $plv1) {
            $ret[$key]['value'][$plk1] = $plv1;
          }
        }
      } else if (!empty($value['anyOf'])) {
        $ret[$key]['subType'] = 'anyOf';

      } else if (!empty($value['oneOf'])) {
        $ret[$key]['subType'] = 'oneOf';

      } else {
        if (is_array($itemTypeOrObject)) {
          $auxSchema          = getSchemaDefinition($itemTypeOrObject[0]);
          $ret[$key]['value'] = genPayloadFromSchemaObject($auxSchema);
        } else {
          echo "\n" . __LINE__ . " How to proceed with this?\n";
          print_r($value);
          die();
        }
      }
    }
  }
  return $ret;
}

function genClassFooter() {
  global $margin;

  $margin -= 2;
  genOutput("}");
}

function expandParameter($parameter, $dieFlag = false) {

  $onlyImportantParamInfo = function ($param) use ($dieFlag) {

    $aux = _getValue($param, 'type', false);
    if (false == $aux) {
      $ret = $param;
    } else {
      $ret['type'] = $aux;
    }

    $otherKeys = ['type', 'enum', 'in', 'example', 'summary', 'description', 'items', '$ref', 'allOf', 'anyOf', 'oneOf'];
    foreach ($otherKeys as $keyName) {
      if (isset($param[$keyName])) {
        $ret[$keyName] = $param[$keyName];
      }
    }

    return $ret;
  };

  $ret = [];
  if (isset($parameter['$ref'])) {
    $auxParams = getSchemaDefinition($parameter);

    $ret         = expandAllParameters(_getValue($auxParams, 'properties', $onlyImportantParamInfo($auxParams)));
    $ret['type'] = _getValue($ret, 'type', 'object');
    if (!empty($auxParams['description'])) {
      if (empty($ret['description'])) {
        $ret['description'] = $auxParams['description'];
      } else {
        $ret['description2'] = $auxParams['description'];
      }
    }

    if ($dieFlag) {
      print_r($ret);
      echo __LINE__ . "\n";
      die();
    }

  } else {
    if (isset($parameter['allOf'])) {
      if ($dieFlag) {
        print_r($parameter);
        echo __LINE__ . "\n";
      }
      foreach ($parameter['allOf'] as $paramSchema) {
        $aux = expandParameter($paramSchema);
        // if ($dieFlag)
        //   die(print_r($aux));
        foreach ($aux as $key => $value) {
          $ret[$key] = $value;
        }
      }
    } else {
      if ($dieFlag) {
        echo __LINE__ . "\n";
        die("------\n");
      }
      $ret = $onlyImportantParamInfo($parameter);
    }
  }
  return $ret;
};

function expandAllParameters($parametersDef) {
  $ret = [];
  foreach ($parametersDef as $key => $value) {
    if ($GLOBALS['debug_0']) {
      echo "** $key\n";
    }

    if (empty($ret[$key])) {
      $ret[$key] = expandParameter($value);
      if ($key == "otherIdentityDocumentNumberXXX") {
        print_r($ret);
        echo __LINE__ . "\n";
        die();
      }
    }
  }
  return $ret;
}

function genPathParams($pathParameters, &$parametersDef, &$parameterList, $cleanup = true) {
  if ($cleanup) {
    if ($GLOBALS['debug_0']) {
      echo "Cleaning-up parameters\n";
    }

    $parametersDef = [];
    $parameterList = '';
  }
  if (is_array($pathParameters)) {
    foreach ($pathParameters as $key => $value) {
      if (empty($parametersDef[$key])) {
        if ($parameterList > '') {
          $parameterList .= ", ";
        }
        $parameterList .= '$' . $key;
        $parametersDef[$key] = $value;
      }
    }
  }
}

function genParamList($pathParameters, $parameters, &$parametersDef, &$parameterList, $cleanup = true) {
  genPathParams($pathParameters, $parametersDef, $parameterList, $cleanup);

  if (is_array($parameters)) {
    foreach ($parameters as $key => $value) {
      if (empty($parametersDef[$key])) {

        $key = createIdentifierFromDescription($value['name'] ?: $key);

        $__key = mb_strtolower('$' . $key);
        if (!strpos(mb_strtolower($parameterList), mb_strtolower($__key))) {
          if (empty($parametersDef[$key])) {
            if ($parameterList > '') {
              $parameterList .= ", ";
            }
            $parameterList .= $__key;
            $parametersDef[$key] = [
              'type'        => 'string',
              'description' => '',
              'example'     => '',
            ];
            $ref = _getValue($value, '$ref', false);
            if ($ref) {
              $aux = getSchemaDefinition($value);
              foreach ($aux as $key1 => $value1) {
                if (in_array($key1, ['type', 'description', 'example', 'in', 'name'])) {
                  $parametersDef[$key][$key1] = $value1;
                }

              }
              foreach (_getValue($aux, 'schema') as $key2 => $value2) {
                $parametersDef[$key][$key2] = $value2;
              }
            }
          }
        }
      }
    }
  } else {
    // die(print_r($parameters));
  }
}

function improveParamNames(&$parameters) {
  $ret = [];
  if (is_array($parameters)) {
    foreach ($parameters as $key => $value) {
      $newKey = $key;
      if (is_int($key)) {
        $ref = _getValue($value, '$ref', false);
        if ($ref) {
          $newKey = substr($ref, strrpos($ref, '/') + 1);
        }
      }
      $ret[$newKey] = $value;
    }
  }
  $parameters = $ret;
}

global $apiEntryPoints;
$apiEntryPoints = [];

function genProducer($targetFilename, $jSource, $parentClassName = '') {
  global $margin, $tags, $components,
  $genComment,
  $openComment, $closeComment,
  $marginDeep,
  $apiEntryPoints, $basicReturnClassName;

  if (genClassHeader($targetFilename, $jSource, $parentClassName)) {
    getTags($jSource);
    $components = _getValue($jSource, 'components', []);
    $paths      = _getValue($jSource, 'paths', []);

    $allowedMethods = ['get', 'delete', 'post', 'put', 'patch'];

    foreach ($paths as $path => $methodsDef) {

      $pathParameters = [];
      preg_match_all('/{[a-zA-Z0-9_]*}/', $path, $pathContainParameters);
      if ($pathContainParameters[0]) {
        foreach ($pathContainParameters[0] as $key) {
          $key2                  = preg_replace('/{([a-zA-Z0-9_]*)}|:([a-zA-Z0-9_]*)/', '$1$2', $key);
          $pathParameters[$key2] = [
            'type'   => 'string',
            'source' => $key,
          ];

          $path = str_replace(":$key2", "\$$key2", $path);
          $path = str_replace('{' . $key2 . '}', "\$$key2", $path);
        }
      }

      $parameters = _getValue($methodsDef, 'parameters', []);
      improveParamNames($parameters);
      genParamList($pathParameters, $parameters, $sectionParametersDef, $sectionParametersList);

      foreach ($methodsDef as $method => $methodLoad) {
        if (in_array($method, $allowedMethods)) {
          $functionName = _getValue($methodLoad, 'operationId', createIdentifierFromDescription(_getValue($methodLoad, 'summary')));
          echo "$method $path $functionName()\n";

          $parametersList = $sectionParametersList;
          $parametersDef  = $sectionParametersDef;

          $requestBody = _getValue($methodLoad, "requestBody", false);
          if ($requestBody) {
            if (isset($requestBody['content'])) {
              $mime = arrayKeyFirst($requestBody['content']);
              if ($mime == "application/json") {
                $schema           = _getValue($requestBody['content'][$mime], 'schema', []);
                $schemaDefinition = getSchemaDefinition($schema);

                genPathParams($pathParameters, $parametersDef, $parametersList);

                foreach (_getValue($schemaDefinition, 'properties', []) as $paramName => $paramDef) {
                  if (empty($parametersDef[$paramName])) {
                    if ($parametersList > '') {
                      $parametersList .= ", ";
                    }

                    $parametersList .= '$' . $paramName;
                    $parametersDef[$paramName] = $paramDef;
                  }
                }

              } else {
                die("How o proceed with mime: '$mime'\n");
              }
            } else {
              die(print_r($requestBody));
            }
          } else {
            $parameters = _getValue($methodLoad, "parameters", false);
            improveParamNames($parameters);
            genParamList($pathParameters, $parameters, $parametersDef, $parametersList);
          }

          $openComment();
          _getValueWithCallback($methodLoad, "summary", "", $genComment, "");
          _getValueWithCallback($methodLoad, "description", "", $genComment, "");
          $genComment("@method $method");
          $marginDeep += 2;

          $paramDef2 = expandAllParameters($parametersDef);

          genParamComment($paramDef2);

          // if ($functionName=="createPurchase")
          //   die(print_r($paramDef2));

          $marginDeep -= 2;
          $closeComment();

          $responses = _getValue($methodLoad, "responses", false);

          genOutput("public function $functionName($parametersList) {");
          $margin += 2;

          $innerDescription = _getValue($requestBody, "description", "");
          if ($innerDescription > '') {
            $openComment();
            $genComment($innerDescription);
            $closeComment();
          }

          genOutput("\$ret = new $basicReturnClassName();");
          genOutput("\$http_result = 500;");

          $openComment();
          $genComment("Your process goes here.\nLeave the result at http_result variable\nBy default, it will answer with 500");
          $closeComment();

          $openComment();
          $genComment("Treatment of responses as defined in the OpenAPI file.\nThis is the way in which the customer (consumer) is waiting for the answers.\nThe responses below are those taken from the examples and description in the ");
          $closeComment();
          genOutput("\$ret->http_code = \$http_result;");

          if ($responses) {
            genOutput("switch (\$http_result) {");
            $margin += 2;
            foreach ($responses as $key => $value) {
              if ("default" == $key) {
                genOutput("$key: {");
              } else {
                genOutput("case $key: {");
              }

              $margin += 2;

              genOutput("\$ret->message = '" . $value['description'] . "';");
              $retContent = '';
              $content    = _getValue($value, 'content', false);

              foreach ($content as $contentType => $contentDefinition) {
                $schema = _getValue($contentDefinition, 'schema', false);
                if ($schema) {
                  $genResponse = function ($auxSchema) {
                    global $margin;
                    $retContentAux = "";
                    if (!empty($auxSchema['$ref'])) {
                      $retItems = _getValue(getSchemaDefinition($auxSchema), 'properties', []);

                      $maxLen = 0;
                      foreach ($retItems as $retName => $retDef) {
                        if (strlen($retName) > $maxLen) {
                          $maxLen = strlen($retName);
                        }

                      }

                      foreach ($retItems as $retName => $retDef) {
                        $retContType = mb_strtolower(_getValue($retDef, 'type', 'UNDEFINED'));
                        if (!empty($retDef['example'])) {
                          $retContValue = $retDef['example'];
                          if ($retContType == 'string') {
                            $retContValue = "'$retContValue'";
                          }

                        } else {
                          $retContValue = "'WARN $retContType value is required'";
                        }
                        if ($retContentAux > '') {
                          $retContentAux .= ",\n" . str_repeat(" ", $margin + 2);
                        }

                        $spaces = str_repeat(" ", $maxLen - strlen($retName));

                        if (is_array($retContValue)) {
                          $retContValue = json_encode($retContValue);
                        }

                        $retContentAux .= "'$retName' $spaces=> $retContValue";
                      }
                    } else {

                    }
                    return "[\n" . str_repeat(" ", $margin + 2) . $retContentAux . "\n" . str_repeat(" ", $margin) . "]";
                  };
                  $retContent = $genResponse($schema);
                }
              }
              genOutput("\$ret->content = json_encode($retContent);");

              $margin -= 2;
              genOutput("}\n");
            }
            $margin -= 2;
            genOutput("}\n");
          }

          genOutput("return \$ret;");

          $margin -= 2;
          genOutput("}\n");

          $apiEntryPoints[$functionName] = [
            'method'     => $method,
            'path'       => $path,
            'parameters' => $paramDef2,
          ];

        } else {
          if ($method != "parameters") {
            die("$method unknown method\n");
          }

        }
      }

    }
    genOutput("function __construct(\$api) {");
    $margin += 2;
    if ($parentClassName > '') {
      genOutput("parent::__construct(\$api);");
    }

    genOutput("\$this->__api=\$api;");
    foreach ($apiEntryPoints as $functionName => $functionDef) {
      genOutput("\n");
      genOutput("// $functionName");
      genOutput("\$api->registerEntry(");
      $margin += 2;
      genOutput("'$functionName',");
      genOutput("'" . mb_strtoupper($functionDef['method']) . "',");
      genOutput("'" . $functionDef['path'] . "',");
      {
        $margin += 2;
        genOutput("'" . str_replace("'", "\\'", (str_replace("\\n", "\n" . str_repeat(" ", $margin), json_encode($functionDef['parameters'], JSON_PRETTY_PRINT | JSON_NUMERIC_CHECK)))) . "'");
        $margin -= 2;
      }
      $margin -= 2;
      genOutput(");");
    }
    $margin -= 2;
    genOutput("}");

    genClassFooter();
  }
}

function genConsumer($targetFilename, $jSource, $parentClassName = '') {
  global $margin, $tags, $components,
  $genComment,
  $openComment, $closeComment,
  $marginDeep,
  $apiEntryPoints, $basicReturnClassName;

  if (genClassHeader($targetFilename, $jSource, $parentClassName)) {
    getTags($jSource);
    $components = _getValue($jSource, 'components', []);
    $paths      = _getValue($jSource, 'paths', []);

    $allowedMethods = ['get', 'delete', 'post', 'put', 'patch'];

    $sectionParametersDef  = [];
    $sectionParametersList = '';

    foreach ($paths as $path => $methodsDef) {

      $pathParameters = [];
      preg_match_all('/{[a-zA-Z0-9_]*}/', $path, $pathContainParameters);
      if ($pathContainParameters[0]) {
        foreach ($pathContainParameters[0] as $key) {
          $key2                  = preg_replace('/{([a-zA-Z0-9_]*)}|:([a-zA-Z0-9_]*)/', '$1$2', $key);
          $pathParameters[$key2] = [
            'type'   => 'string',
            'source' => $key,
          ];

          $path = str_replace(":$key2", "\$$key2", $path);
          $path = str_replace('{' . $key2 . '}', "\$$key2", $path);
        }
      }

      $parameters = _getValue($methodsDef, 'parameters', []);
      improveParamNames($parameters);
      genParamList($pathParameters, $parameters, $sectionParametersDef, $sectionParametersList);

      foreach ($methodsDef as $method => $methodLoad) {
        if (in_array($method, $allowedMethods)) {
          $functionName =
            _getValue(
            $methodLoad,
            'operationId',
            createIdentifierFromDescription(
              _getValue(
                $methodLoad,
                'summary'
              )
            )
          );
          if ($functionName == '') {
            $functionName = createIdentifierFromDescription(
              $path
            ) . mb_strtoupper($method);
          }

          $parametersList = $sectionParametersList;
          $parametersDef  = $sectionParametersDef;

          echo "$method $path $functionName()\n";

          $cleanupParameters = true;
          $requestBody       = _getValue($methodLoad, "requestBody", false);
          if ($requestBody) {
            if (isset($requestBody['content'])) {
              $cleanupParameters = false;
              foreach (array_keys($requestBody['content']) as $mime) {
                if ($mime == "application/json") {
                  $schema           = _getValue($requestBody['content'][$mime], 'schema', []);
                  $schemaDefinition = getSchemaDefinition($schema);

                  genPathParams($pathParameters, $parametersDef, $parametersList);

                  foreach (_getValue($schemaDefinition, 'properties', []) as $paramName => $paramDef) {
                    if (empty($parametersDef[$paramName])) {

                      if ($parametersList > '') {
                        $parametersList .= ", ";
                      }

                      $parametersList .= '$' . $paramName;
                      if (!empty($paramDef['$ref'])) {
                        $parametersDef[$paramName] = _getValue(getSchemaDefinition($paramDef), 'properties', []);
                      } else {
                        $parametersDef[$paramName] = $paramDef;
                      }
                    }

                  }

                } else {
                  echo "\n" . __LINE__ . " WARN: How to proceed with mime: '$mime'\n";
                }
              }
              // $mime              = arrayKeyFirst($requestBody['content']);
            } else {
              die(print_r($requestBody));
            }
          }

          if ($GLOBALS['debug_0']) {
            echo "cleanupParameters = " . ($cleanupParameters ? 'TRUE' : 'FALSE') . "\n";
          }

          $parameters = _getValue($methodLoad, "parameters", false);
          improveParamNames($parameters);
          genParamList($pathParameters, $parameters, $parametersDef, $parametersList, $cleanupParameters);

          $openComment();
          _getValueWithCallback($methodLoad, "summary", "", $genComment, "");
          _getValueWithCallback($methodLoad, "description", "", $genComment, "");
          $genComment("@method $method");
          $marginDeep += 2;

          $paramDef2 = expandAllParameters($parametersDef);

          genParamComment($paramDef2);

          $marginDeep -= 2;
          $closeComment();

          // if ($functionName == "updateAccount") {
          //   echo ">>> pathParameters\n";
          //   print_r($pathParameters);
          //   echo ">>> parameters\n";
          //   print_r($parameters);
          //   echo ">>> parametersList\n";
          //   print_r($parametersList);
          //   echo "\n>>> parametersDef\n";
          //   print_r($parametersDef);
          //   echo ">>> paramDef2\n";
          //   print_r($paramDef2);
          //   die();
          // }

          $responses = _getValue($methodLoad, "responses", false);

          genOutput("public function $functionName($parametersList) {");
          $margin += 2;

          $innerDescription = _getValue($requestBody, "description", "");
          if ($innerDescription > '') {
            $openComment();
            $genComment($innerDescription);
            $closeComment();
          }

          $requestCount = 0;
          if (count($parameters) > 0) {
            $openComment();
            $genComment("PARAMETERS");
            $genComment(json_encode($parametersDef, JSON_PRETTY_PRINT));
            $closeComment();
          }

          $questionMarkSolved = (false !== strpos($path, "?"));
          $headerParameters   = [];

          foreach ($parametersDef as $paramName => $paramDef) {
            $paramPlace = _getValue($paramDef, 'in', false);
            if ($paramPlace == 'query') {
              if (!$questionMarkSolved) {
                $path .= "?";
                $questionMarkSolved = true;
              } else {
                $path .= "&";
              }
              $path .= _getValue($paramDef, 'name', 'UNKNOWN') . "=\$$paramName";
            }

            if ($paramPlace == 'header') {
              $headerParameters[$paramName] = "\$$paramName";
            }
          }

          $jHeaderParameters = json_encode($headerParameters);

          $pathUsed = [];

          foreach (_getValue($requestBody, 'content', []) as $mime => $mimeSchema) {
            if (addPath($method, $path)) {
              $auxSchema  = getSchemaDefinition($mimeSchema['schema']);
              $auxPayload = genPayloadFromSchemaObject($auxSchema);
              $openComment();
              $genComment("Payload preparation");
              $closeComment();

              genOutput("\$payload = [");
              $margin += 2;
              genArrayPayloadOutput($auxPayload);
              $margin -= 2;
              genOutput("];");

              // if ($functionName == 'addAccount') {
              //   echo "\n=========================================\n";
              //   print_r($auxPayload);
              //   die();
              // }

              // genOutput(json_encode($auxPayload, JSON_PRETTY_PRINT));
              $openComment();
              $genComment("Endpoint call");
              $closeComment();
              genOutput("\$ret = callServer('$jHeaderParameters', '$method', \"$path\", '$mime', \$payload);");
              $requestCount++;
            }
          }

          if ($requestCount == 0) {
            $openComment();
            $genComment("Endpoint call");
            $closeComment();
            genOutput("\$ret = callServer('$jHeaderParameters', '$method', \"$path\");");
          }

          if ($responses) {
            $openComment();
            $genComment("Handling responses");
            $closeComment();

            genOutput("switch (\$ret['http_code']) {");
            $margin += 2;
            foreach ($responses as $http_code => $http_response_spec) {
              genOutput("case $http_code: {");
              $description = _getValue($http_response_spec, 'description', false);
              $openComment();
              if ($description) {
                $genComment($http_response_spec['description']);
              }

              $content = _getValue($http_response_spec, 'content', false);
              if ($content) {
                foreach ($content as $mime => $mimeSchema) {
                  $mimeSchemaDef = getSchemaDefinition(_getValue($mimeSchema, 'schema', []));
                  $genComment(json_encode(expandAllParameters(_getValue($mimeSchemaDef, 'properties', [])), JSON_PRETTY_PRINT));
                }
              }
              $closeComment();
              genOutput("}");
            }
            $margin -= 2;
            genOutput("}");
          }

          genOutput("return \$ret;");

          $margin -= 2;
          genOutput("}\n");

          $apiEntryPoints[$functionName] = [
            'method'     => $method,
            'path'       => $path,
            'parameters' => $paramDef2,
          ];

        } else {
          if ($method != "parameters") {
            die("$method unknown method\n");
          }

        }
      }

    }
    genOutput("function __construct(\$api) {");
    $margin += 2;
    if ($parentClassName > '') {
      genOutput("parent::__construct(\$api);");
    }

    genOutput("\$this->__api=\$api;");

    $margin -= 2;
    genOutput("}");

    genClassFooter();
  }
}

$isCLI                = (php_sapi_name() == "cli");
$asConsumer           = null;
$asProducer           = null;
$classIdentifier      = '';
$basicReturnClassName = '';
$author               = '';
$mainFilename         = '';

$options = getopt(
  "a:f:k:p:h:c:r:",
  [
    "author:",
    "file:",
    "consumer::",
    "producer::",
    "class:",
    "basicReturn:",
    "help:",
  ]);
$displayHelp = false;
foreach ($options as $key => $value) {
  echo "\t$key: $value\n";
  switch ($key) {

  case 'a':
  case 'author':
    $author = $value;
    break;

  case 'c':
  case 'class':
    $classIdentifier = $value;
    break;

  case 'r':
  case 'basicReturn':
    $basicReturnClassName = $value;
    break;

  case 'f':
  case 'file':
    $mainFilename = $value;
    break;

  case 'k':
  case 'consumer':
    $asConsumer = true;
    break;

  case 'p':
  case 'producer':
    $asProducer = true;
    break;

  case 'h':
  case 'help':
    $displayHelp = true;
    break;

  default:
    die("$key é um parâmetro desconhecido\n");
    break;
  }
}

$displayHelp = $displayHelp ||
  ($mainFilename == '') || (($asConsumer || $asProducer) == false);

if ($displayHelp) {
  echo basename($argv[0]) . "\n\t-h\tThis help\n\t-a\t<author>\n\t-f\t<fileName>\n\t[--consumer]\n\t[--producer]\n\t-c <parentClassName>\n\t-r <basicReturnClassName>\n";

  if (($asConsumer || $asProducer) == false) {
    echo "Indicate if this will be a producer or a consumer\n";
  }

  exit(1);
}
$mainFilename = @$options['f'];

if (file_exists($mainFilename)) {
  $jsonSource = json_decode(file_get_contents($mainFilename), true);
  if ($asProducer) {
    genProducer(replaceExtension($mainFilename, ".php"), $jsonSource, $classIdentifier);
  } else if ($asConsumer) {
    genConsumer(replaceExtension($mainFilename, ".php"), $jsonSource, $classIdentifier);
  }
} else {
  die("File $mainFilename not found\n");
}
